# player class
package Pong::Player;

use strict;
use warnings;
use v5.10;

use Moo;

extends 'Pong::Object';

# player's paddle (or platform)
has paddle => (
               is       => 'rw',
               required => 1
              );

# player's score.
has score => (
              is       => 'rw',
              required => 0,
              writer   => 'set_score'
             );

1;

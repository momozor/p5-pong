p5-pong
--------
A classic Pong game in Perl 5 and SDL. Featuring AI, leaderboard and and simple graphics.

![](https://user-images.githubusercontent.com/24475030/28744287-f9cf649e-748f-11e7-9a64-dce5dee935e6.gif)

# Getting Started

## Prerequisites
* cpanm

## Installation
* With ***cpanm***:

```sh

# Works on linux and windows
cpanm SDL --verbose
cpanm --installdeps .

```

## Running

    perl pong.pl
    
* You can quit the game by clicking 'x' at top of game window or by pressing *q* key.

Acknowledgements
-----------------

> The original Perl implementation is authored by Breno G. de Oliveira (garu)

Special thanks to Perl hackers at Freenode #perl for guiding me through the development on this game, and thank you Perl's SDL developer team for the game tutorial and implementing a great binding to Simple Direct Media library.

This project is licensed under GPL3 license - See LICENSE file for details.
